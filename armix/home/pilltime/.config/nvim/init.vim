set termguicolors
set number relativenumber
set guicursor=n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20,a:blinkwait500-blinkon250-blikoff250-Cursor/lCursor

syntax on
au VimLeave * set guicursor=a:hor20-blinkon200
