setopt appendhistory autocd beep promptsubst interactivecomments correct histignorespace completealiases
bindkey -e
zstyle ':compinstall' filename $ZDOTDIR/.zshrc
zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
autoload -Uz compinit colors
compinit; colors; tabs -4

export GPG_TTY=$TTY

################################################################################

alias _='sudo'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'
alias less='less -R'
alias figlet='figlet -C utf8'

alias yt-video='yt-dlp -o "$HOME/Videos/%(title)s.%(ext)s" --'
alias yt-music='yt-dlp -x -o "$HOME/Music/%(title)s.%(ext)s" --'
alias yt-music-mp3='yt-dlp -x --audio-format mp3 -o "$HOME/Music/%(title)s.%(ext)s" --'
alias yt-search='yt-dlp -e --get-id --get-duration --default-search "ytsearch10:" --'

music() {
    mpv \
      --keep-open \
      --loop-playlist \
      --shuffle \
      --volume=${1:-40} \
      --term-osd-bar \
      --term-playing-msg='${media-title}' \
      --ytdl-raw-options=format=${2:-ba} \
      'https://www.youtube.com/playlist?list=PLu5QwTJVICXVHtkCjyhtnA9vaLz8_Mkhz'
}

lofi() {
    mpv \
      --msg-level=ffmpeg=no \
      --quiet \
      --volume=${1:-40} \
      --no-video \
      'https://youtu.be/jfKfPfyJRdk'
}

burn_usb() {
    sudo dd if=$1 of=$2 bs=${3:-4M} oflag=sync conv=fsync status=progress
}

coolio() {
    figlet -tcf slant "$1" | lolcat -tas 32 -d 8
}

################################################################################

PROMPT_COLOR=green; [ $UID -eq 0 ] && PROMPT_COLOR=red

PROMPT=' %{$fg[blue]%}%m%{$reset_color%} %{$fg[$PROMPT_COLOR]%}$(pwd | sed -E "s|$HOME|~| ; s|((\s*\.?\s*[^/]\s*[^/]\s*[^/])\s*[^/]*/)|\2/|g") %{$reset_color%}%(!.#.>) '

RPROMPT=' %{$fg[red]%}%(?..%?)%{$reset_color%}'

################################################################################

ZSH_PLUGINS=/usr/share/zsh/plugins

source $ZSH_PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH_PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH_PLUGINS/zsh-history-substring-search/zsh-history-substring-search.zsh

bindkey '^U'      backward-kill-line
bindkey '^[[A'    history-substring-search-up
bindkey '^[[B'    history-substring-search-down
bindkey '^[[C'    forward-char
bindkey '^[[D'    backward-char
bindkey '^[[H'    beginning-of-line
bindkey '^[[F'    end-of-line
bindkey '^[[1~'   beginning-of-line
bindkey '^[[4~'   end-of-line
bindkey '^[[3~'   delete-char
bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word
